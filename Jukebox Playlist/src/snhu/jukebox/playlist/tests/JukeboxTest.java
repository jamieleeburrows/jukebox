package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(2, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	
	@Test // Hozier
	public void testGetHoziersAlbumSize() throws NoSuchFieldException, SecurityException {
		 Hozier hozier = new Hozier();
		 ArrayList<Song> hoziersTracks = new ArrayList<Song>();
		 hoziersTracks = hozier.getHozierSongs();
		 assertEquals(2, hoziersTracks.size());
	}
	@Test // Jimmy Eat World
	public void testJimmyEWAlbumSize() throws NoSuchFieldException, SecurityException {
		 JimmyEatWorld jimmyeatworld = new JimmyEatWorld();
		 ArrayList<Song> JimmyEatWorldTracks = new ArrayList<Song>();
		 JimmyEatWorldTracks = jimmyeatworld.getJimmyEatWorldSongs();
		 assertEquals(3, JimmyEatWorldTracks.size());
	}
	
	

	@Test // Coldplay
	public void testColdplayAlbumSize() throws NoSuchFieldException, SecurityException {
		Coldplay coldplay = new Coldplay();
		ArrayList<Song> ColdplayTracks = new ArrayList<Song>();
		ColdplayTracks = coldplay.getColdplaySongs();
		assertEquals(3, ColdplayTracks.size());
}


	@Test // Amos Lee
	public void testAmosLeeAlbumSize() throws NoSuchFieldException, SecurityException {
		AmosLee amoslee = new AmosLee();
		ArrayList<Song> AmosLeeTracks = new ArrayList<Song>();
		AmosLeeTracks = amoslee.getAmosLeeSongs();
		assertEquals(2, AmosLeeTracks.size());
}

	@Test // Tool
	public void testToolAlbumSize() throws NoSuchFieldException, SecurityException {
		Tool Tool = new Tool();
		ArrayList<Song> ToolTracks = new ArrayList<Song>();
		ToolTracks = Tool.getToolSongs();
		assertEquals(3, ToolTracks.size());
}

	@Test // The Misfits
	public void testTheMisfitsAlbumSize() throws NoSuchFieldException, SecurityException {
		TheMisfits TheMisfits = new TheMisfits();
		ArrayList<Song> TheMisfitsTracks = new ArrayList<Song>();
		TheMisfitsTracks = TheMisfits.getTheMisfitsSongs();
		assertEquals(2, TheMisfitsTracks.size());
}


	
	
	
}
