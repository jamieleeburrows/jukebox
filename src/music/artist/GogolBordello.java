package music.artist;

import snhu.jukebox.playlist.Song;
import java.util.ArrayList;

public class GogolBordello {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public GogolBordello() {
    }
    
    public ArrayList<Song> getGogolBordelloSongs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("When Universes Collide", "Gogol Bordello");   //Create a song
         Song track2 = new Song("Undestructable", "Gogol Bordello");         	//Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for Gogol Bordello
         this.albumTracks.add(track2);                                          //Add the second song to song list for Gogol Bordello 
         return albumTracks;                                                    //Return the songs for Gogol Bordello in the form of an ArrayList
    }
}
