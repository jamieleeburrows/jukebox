package snhu.jukebox.playlist.tests;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;
import music.artist.*;
import snhu.jukebox.playlist.Song;

public class JukeboxTest {

	@Test
	public void testGetBeatlesAlbumSize() throws NoSuchFieldException, SecurityException {
		 TheBeatles theBeatlesBand = new TheBeatles();
		 ArrayList<Song> beatlesTracks = new ArrayList<Song>();
		 beatlesTracks = theBeatlesBand.getBeatlesSongs();
		 assertEquals(3, beatlesTracks.size());
	}
	
	@Test
	public void testGetImagineDragonsAlbumSize() throws NoSuchFieldException, SecurityException {
		 ImagineDragons imagineDragons = new ImagineDragons();
		 ArrayList<Song> imagineDragonsTracks = new ArrayList<Song>();
		 imagineDragonsTracks = imagineDragons.getImagineDragonsSongs();
		 assertEquals(3, imagineDragonsTracks.size());
	}
	
	@Test
	public void testGetAdelesAlbumSize() throws NoSuchFieldException, SecurityException {
		 Adele adele = new Adele();
		 ArrayList<Song> adelesTracks = new ArrayList<Song>();
		 adelesTracks = adele.getAdelesSongs();
		 assertEquals(3, adelesTracks.size());
	}
	
	@Test // Hozier
	public void testGetHoziersAlbumSize() throws NoSuchFieldException, SecurityException {
		 Hozier hozier = new Hozier();
		 ArrayList<Song> hoziersTracks = new ArrayList<Song>();
		 hoziersTracks = hozier.getHozierSongs();
		 assertEquals(2, hoziersTracks.size());
	}
	@Test // Jimmy Eat World
	public void testJimmyEWAlbumSize() throws NoSuchFieldException, SecurityException {
		 JimmyEatWorld jimmyeatworld = new JimmyEatWorld();
		 ArrayList<Song> JimmyEatWorldTracks = new ArrayList<Song>();
		 JimmyEatWorldTracks = jimmyeatworld.getJimmyEatWorldSongs();
		 assertEquals(3, JimmyEatWorldTracks.size());
	}	

	@Test // Coldplay
	public void testColdplayAlbumSize() throws NoSuchFieldException, SecurityException {
		Coldplay coldplay = new Coldplay();
		ArrayList<Song> ColdplayTracks = new ArrayList<Song>();
		ColdplayTracks = coldplay.getColdplaySongs();
		assertEquals(3, ColdplayTracks.size());
}
	@Test // Amos Lee
	public void testAmosLeeAlbumSize() throws NoSuchFieldException, SecurityException {
		AmosLee amoslee = new AmosLee();
		ArrayList<Song> AmosLeeTracks = new ArrayList<Song>();
		AmosLeeTracks = amoslee.getAmosLeeSongs();
		assertEquals(2, AmosLeeTracks.size());
}

	@Test // Gogol Bordello
	public void testGogolBordelloAlbumSize() throws NoSuchFieldException, SecurityException {
		GogolBordello gogolbordello = new GogolBordello();
		ArrayList<Song> GogolBordelloTracks = new ArrayList<Song>();
		GogolBordelloTracks = gogolbordello.getGogolBordelloSongs();
		assertEquals(2, GogolBordelloTracks.size());
	}
	
	@Test //Rancid
	public void testRancidAlbumSize() throws NoSuchFieldException, SecurityException {
		Rancid rancid = new Rancid();
		ArrayList<Song> RancidTracks = new ArrayList<Song>();
		RancidTracks = rancid.getRancidSongs();
		assertEquals(3, RancidTracks.size());
	}
	
	@Test //Linkin Park
	public void testLinkinParkAlbumSize() throws NoSuchFieldException, SecurityException {
		LinkinPark linkinPark = new LinkinPark();
		ArrayList<Song> LinkinParkTracks = new ArrayList<Song>();
		LinkinParkTracks = linkinPark.getLinkinParkSongs();
		assertEquals(3, LinkinParkTracks.size());
	}
	
	@Test //GunsNRoses
	public void testGunsNRosesAlbumSize() throws NoSuchFieldException, SecurityException {
		GunsNRoses gunsNRoses = new GunsNRoses();
		ArrayList<Song> GunsNRosesTracks = new ArrayList<Song>();
		GunsNRosesTracks = gunsNRoses.getGunsNRosesSongs();
		assertEquals(3, GunsNRosesTracks.size());
	}

}
